[![License WTFPL](https://img.shields.io/badge/License-wtfpl-lightgrey.svg)](http://www.wtfpl.net/)

# dotfiles #

* [Dotfiles Creed](#dotfiles-creed)
* [License](#license)

### Dotfiles Creed ###

>This is my dotfile repo. There are many like it, but this one is mine.

>My dotfile repo is my best friend. It is my life. I must maintain it as I must 
>maintain my life.

>Without me, my dotfile repo is useless. Without my dotfile repo, I am useless. 
>I must `git commit` straighter than my enemy who is trying to `git blame` me. I 
>must `git push origin master` before he `git clone`'s me. I will...

>My dotfile repo and I know that what counts in war is not the number files we 
>`git add`, the `$SHELL` we use, nor the [README](/README.md) we make. We know 
>that it is the configs that count. We will config...

>My dotfile repo is human, even as I, because it is my life. Thus, I will learn 
>it as a brother. I will learn its weaknesses, its strength, its submodules, its 
>directories, its revisions and its commit history. I will keep my dotfile repo 
>clean and ready, even as I am clean and ready. We will become part of each 
>other. We will...

>Before `$DEITY`, I swear this creed. My dotfile repo and I are the defenders of 
>my `$HOSTNAME`. We are the maintainers of our enemy. We are the maintainers of 
>my life.

>So be it, until victory is `$EDITOR`'s and there is no enemy, but peace!


### License ###

The files within this repository are licensed under the WTFPLv2 unless 
otherwise stated.

Copyright © 2017 B.A. Parker \<commando.brando2k@gmail.com\>
This work is free. You can redistribute it and/or modify it under the terms of 
the Do What The Fuck You Want To Public License, Version 2, as published by Sam 
Hocevar. See http://www.wtfpl.net/ for more details.

