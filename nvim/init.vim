" $HOME/.config/nvim/init.vim

set undofile           " keep an undo file (undo changes after closing)
set ruler              " show the cursor position all the time
set showcmd            " display incomplete commands

function! DoRemote(arg)
    UpdateRemotePlugins
endfunction

" TODO: cleanup
call plug#begin('~/.local/share/nvim/bundle')
  " Completion
  "Plug 'Shougo/deoplete.nvim', {'do': function('DoRemote')}
  "Plug 'zchee/deoplete-clang'
  "Plug 'Valloric/YouCompleteMe', {'do': 'python2 install.py --clang-completer --omni-completer --system-libclang'}
  "Plug 'rdnetto/YCM-Generator', {'branch': 'stable'}
  Plug 'justmao945/vim-clang'

  " Linting
  Plug 'neomake/neomake'
  Plug 'vhdirk/vim-cmake'

  Plug 'ervandew/supertab'
  Plug 'metakirby5/codi.vim'

  " File/project browsing
  Plug 'scrooloose/nerdtree'
  Plug 'Xuyuanp/nerdtree-git-plugin'

  " Syntax
  Plug 'quabug/vim-gdscript'

  " Colorschemes
  Plug 'dylanaraps/wal'
  
  " Be sensible
  Plug 'tpope/vim-sensible'
call plug#end()

" Don't litter the filesystem with backups
if has("vms")
	set nobackup
else
	set backup
	set backupdir=~/.local/share/nvim/backup
	set backupdir+=/tmp
endif

set number                  " 
set relativenumber          " Line numbers from cursor position

set expandtab		        " Insert spaces when tab is pressed
set tabstop=4		        " tabs use 4 spaces
set shiftwidth=4	        " indents use 4 spaces

set encoding=utf-8
set listchars=tab:¶\ ,eol:¬ " Pretty non-printing characters

set laststatus=2            " Statusbar

set foldmethod=syntax       " folding
set nofoldenable            " 
set foldlevel=1             " 

syntax on
filetype plugin indent on   " Filetype detection

" Switch syntax highlighting on, when the terminal has colors
" Also switch on highlighting the last used search pattern.
if &t_Co > 2 || has("gui_running")
	syntax on
	set hlsearch
endif

" Colors
set background=dark

" Only set colorscheme if it's available
if len(globpath(&rtp, 'colors/wal.vim')) > 0
    colorscheme wal
else
    colorscheme default
endif

" Save a file using sudo, so I don't need elevated privilages to run vim.
cmap w!! %!sudo tee > /dev/null %

" Map the leader to space
let mapleader="\<SPACE>"

" Toggle between showing and hiding non-printing characters with \l
nmap <leader>l :set list!<CR>

" Toggle folds
nmap <leader>f za

" Don't use Ex mode, use Q for formatting
noremap Q gq

" Neomake
nnoremap <C-b> :w<cr>:Neomake<cr>
autocmd! BufWritePost * Neomake
let g:neomake_open_list = 2
let g:neomake_cpp_lang_maker = {
        \ 'args': ['-fsyntax-only', '-std=c++14', '-Wall', '-Wextra'],
        \ 'errorformat': '%-G%f:%s:,' .
                       \ '%f:%l:%c: %trror: %m,' .
                       \ '%f:%l:%c: %tarning: %m,' .
                       \ '%f:%l:%c: %m,' .
                       \ '%f:%l: %trror: %m,' .
                       \ '%f:%l: %tarning: %m,' .
                       \ '%f:%l: %m',
                       \ }
let g:neomake_cpp_enabled_makers=['clang']

" YouCompleteMe
let g:ycm_confirm_extra_conf=1
let g:ycm_extra_conf_globlist = ['~/code/*', '!~/*']

" deoplete
let g:deoplete#enable_at_startup=1
"inoremap <silent><expr> <tab> pumvisible() ? "\<c-n> : deoplete#mappings#manual_complete()
let g:deoplete#sources#clang#libclang_path="/usr/lib/libclang.so"
let g:deoplete#sources#clang#clang_header="/usr/include/clang/"

" CTRL-U in insert mode deletes a lot.  Use CTRL-G u to first break undo,
" so that you can undo CTRL-U after inserting a line break.
inoremap <C-U> <C-G>u<C-U>

" vim-plug
augroup vimplug
    autocmd!

    " Auto-install missing plugins on startup
    " TODO: PlugInstall exits before all missing plugins are installed if
    "       I attempt to autoclose
    "       PlugInstall --sync (which should block until finished) exits with
    "       'No plugin to install'
    autocmd VimEnter *
      \ if len(filter(values(g:plugs), '!isdirectory(v:val.dir)')) | 
      \     PlugInstall | "q |  
      \ endif
 
augroup END

" Put these in an autocmd group, so that we can delete them easily.
augroup vimrcEx
  autocmd!

  " For all text files set 'textwidth' to 78 characters.
  autocmd FileType text setlocal textwidth=78

  " When editing a file, always jump to the last known cursor position.
  " Don't do it when the position is invalid or when inside an event handler
  autocmd BufReadPost *
    \ if line("'\"") >= 1 && line("'\"") <= line("$") |
    \   execute "normal! g`\"" |
    \ endif

augroup END

" Convenient command to see the difference between the current buffer and the
" file it was loaded from, thus the changes you made.
" Only define it when not defined already.
if !exists(":DiffOrig")
  command DiffOrig vert new | set buftype=nofile | read ++edit # | 0d_ | diffthis
                 \ | wincmd p | diffthis
endif
