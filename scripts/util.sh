# util.sh
# Common shell functions

# Colors
_col_bold=$(tput bold)
_col_norm=$(tput sgr0)
_col_err=$(tput setaf 1)
_col_info=$(tput setaf 4)
_col_warn=$(tput setaf 5)

# output messages
_msg_info="INFO:"
_msg_warn="WARNING:"
_msg_err="ERROR:"

info() {
    local msg="${_col_info}$_msg_info${_col_norm} $@"
    printf '%s\n' $msg
}

warn() {
    local msg="${_col_warn}$_msg_warn${_col_norm} $@"
    printf '%s\n' $msg
}

err() {
    local msg="${_col_err}$_msg_err${_col_norm} $@"
    printf '%s\n' $msg
}

die() {
    local stat=$1
    shift
    err "$@" >&2
    exit "$stat"
}

